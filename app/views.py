from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import ListView, DeleteView
from django.urls import reverse_lazy
from queue import PriorityQueue
from .forms import *
from .models import *


def vista(request):
    if not request.user.is_authenticated:
        return redirect('login')
    return render(request, 'inicio.html')


def cerrar(request):
    logout(request)
    return redirect('login')


def iniciar_sesion(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            usuario = request.POST['username']
            clave = request.POST['password']
            user = authenticate(username=usuario, password=clave)
            if user is not None:
                login(request, user)
                return redirect('inicio')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})


def register(request):
    if request.method == 'POST':
        form = CreacionUSuarioForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('inicio')
    else:
        form = CreacionUSuarioForm()
    return render(request, 'registro.html', {'form': form})


def solicitarTurno(request):
    if request.method == 'POST':
        form = SolicitarTurnoForm(request.POST)
        if form.is_valid():
            servicio = form.cleaned_data['servicio']
            servicio_id = Servicios.objects.filter(descripcion=servicio).first().servicio_id

            form.save()
            return redirect('listarTurnosBoxes', servicio_id)
    else:
        form = SolicitarTurnoForm()
    return render(request, 'registro_tickets.html', {'form': form})


class MostrarBoxes(ListView):
    model = Servicios
    template_name = 'mostrar_boxes.html'


# mostrar turnos por boxes
def mostrarTurnoBoxes(request, servicio_id):
    turnos = Tickets.objects.filter(servicio=servicio_id)
    turnos = turnos.order_by('fecha_hora_ingreso_registro')
    servicio = Servicios.objects.get(servicio_id=servicio_id).descripcion
    context = {
        'turnos': turnos,
        'servicio': servicio
    }
    return render(request, 'mostrar_turnos_boxes.html', context)

def eliminarTurno(request, pk):
    turno = Tickets.objects.get(ticket_id=pk)
    servicio_id = turno.servicio.servicio_id
    turno.delete()
    return redirect('listarTurnosBoxes', servicio_id)

