from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [

    path('register/', views.register, name='register'),
    path('login/', views.iniciar_sesion, name='login'),
    path('cerrar/', views.cerrar, name='cerrar'),
    path('', views.vista, name='inicio'),
    path('tickets/', views.solicitarTurno, name='solicitarTurno'),
    path('boxes/', views.MostrarBoxes.as_view(), name='listarboxes'),
    path('boxes/<int:servicio_id>/turnos/',views.mostrarTurnoBoxes, name='listarTurnosBoxes'),
    path('boxes/turnos/eliminar/<pk>',views.eliminarTurno, name='eliminarTurno'),

]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
