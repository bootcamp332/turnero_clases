from django.contrib import admin
from . import models
#from .forms import BoxesModelForm

# Register your models here.


"""class BoxesAdmin(admin.ModelAdmin):
    list_display = ["descripcion", "abreviatura", "esta_activo"]
    list_filter = ["esta_activo"]
    list_editable = ["esta_activo"]
    search_fields = ["descripcion"]
    form = BoxesModelForm
"""

admin.site.register(models.Boxes)
admin.site.register(models.Socios)
admin.site.register(models.Tickets)
admin.site.register(models.Servicios)
admin.site.register(models.Usuarios)
admin.site.register(models.AperturaCierreBoxes)
admin.site.register(models.BoxesServicios)
admin.site.register(models.Estados)
admin.site.register(models.Prioridades)
admin.site.register(models.SiguienteNroServicio)
admin.site.register(models.TicketsServicios)
admin.site.register(models.TicketsSocios)


