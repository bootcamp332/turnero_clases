from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from .models import *


class AccesoForm(AuthenticationForm):
    username = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': 'User name'}))
    password = forms.CharField(widget=forms.PasswordInput({
        'class': 'form-control',
        'placeholder': 'Password'}))

    def __init__(self, *args, **kwargs):
        super(AccesoForm, self).__init__(*args, **kwargs)

        for name, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})


class CreacionUSuarioForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password']

    def __init__(self, *args, **kwargs):
        super(CreacionUSuarioForm, self).__init__(*args, **kwargs)

        for name, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})


class SolicitarTurnoForm(forms.ModelForm):
    class Meta:
        model = Tickets
        labels = {'nro_documento': 'Cédula de Identidad', 'prioridad': 'Condición','servicio': 'Servicio',}
        exclude = ["fecha_hora_ingreso_registro", "cod_ticket"]

    def __init__(self, *args, **kwargs):
        super(SolicitarTurnoForm, self).__init__(*args, **kwargs)
        self.fields['nro_documento'].widget.attrs['placeholder'] = 'Ej: 231345'
        for name, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})


class TicketsServicioForm(forms.ModelForm):
    class Meta:
        model = TicketsServicios
        labels = {'ticket':'Ticket','item':'Item','box_id_atencion': 'Box de Servicio'}
        exclude = ["fecha_hora_ingreso_registro", "fecha_hora_inicio_atencion","fecha_hora_fin_atencion", "estado"]

    def __init__(self, *args, **kwargs):
        super(TicketsServicioForm, self).__init__(*args, **kwargs)

        for name, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})
