Proyecto Turnero 
 Proyecto de turnero web desarrollado con Django y utilizando el sistema de base de datos PostgreSQL 

Proyecto en proceso 

Funcionalidades del proyect 

 Sacar turno: Generación de ticket de atención. 
 Gestión de Cola: ABM de colas de espera. 
 Gestión de Servicios: ABM de tipos de servicios ofrecidos. 


Instrucciones de uso 

 Activar el entorno virtual con el comando respectivo activate. 
 Instalar las dependencias indicadas en requirements.txt. 
 Ejecutar el servidor a través de python manage.py runserver. 
 Iniciar sesión en 127.0.0.1:8000/login con usuario y contraseña admin. 


  ✔ Tecnologías utilizadas 


 Django 
 PostgreSQL 
 Bootstrap
